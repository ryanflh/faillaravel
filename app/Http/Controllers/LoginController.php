<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LoginController extends Controller
{
   public function login()
   {
       return view('auth.login');
   } //

   public function signin(Request $request)
   {
       $firstName = $request->firstName;
       $lastName = $request->lastName;
       $rgender = $request->rgender;
       $nation = $request->nation;
       $boxlanguage = $request->boxlanguage;
       $bio = $request->bio;

       return view('home',compact('firstName','lastName','rgender','nation','boxlanguage','bio'));
   }
}
